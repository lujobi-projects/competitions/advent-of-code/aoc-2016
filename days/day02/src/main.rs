use utils::*;

const CURRENT_DAY: u8 = 2;

pub fn part1() -> i64 {
    let input = read_input!("\n", String);

    let mut x_pos = 1;
    let mut y_pos = 1;

    let mut code = 0;

    for line in input {
        for c in line.chars() {
            match c {
                'U' => {
                    y_pos = (y_pos - 1).max(0);
                }
                'D' => {
                    y_pos = (y_pos + 1).min(2);
                }
                'L' => {
                    x_pos = (x_pos - 1).max(0);
                }
                'R' => {
                    x_pos = (x_pos + 1).min(2);
                }
                _ => panic!("Invalid direction"),
            }
        }
        code *= 10;
        code += y_pos * 3 + x_pos + 1;
    }

    code
}

pub fn part2() -> String {
    let input = read_input!("\n", String);

    let mut x_pos = 0;
    let mut y_pos = 2;

    let mut code = "".to_string();

    for line in input {
        for c in line.chars() {
            match c {
                'U' => {
                    y_pos = (y_pos - 1).max(0);
                    if x_pos == 0 || x_pos == 4 {
                        y_pos = 2;
                    }
                    if x_pos == 1 || x_pos == 3 {
                        y_pos = y_pos.min(3).max(1);
                    }
                }
                'D' => {
                    y_pos = (y_pos + 1).min(4);
                    if x_pos == 0 || x_pos == 4 {
                        y_pos = 2;
                    }
                    if x_pos == 1 || x_pos == 3 {
                        y_pos = y_pos.min(3).max(1);
                    }
                }
                'L' => {
                    x_pos = (x_pos - 1).max(0);

                    if y_pos == 0 || y_pos == 4 {
                        x_pos = 2;
                    }
                    if y_pos == 1 || y_pos == 3 {
                        x_pos = x_pos.min(3).max(1);
                    }
                }
                'R' => {
                    x_pos = (x_pos + 1).min(4);

                    if y_pos == 0 || y_pos == 4 {
                        x_pos = 2;
                    }
                    if y_pos == 1 || y_pos == 3 {
                        x_pos = x_pos.min(3).max(1);
                    }
                }
                _ => panic!("Invalid direction"),
            }
        }
        code += match (x_pos, y_pos) {
            (0, 2) => "5",
            (1, 1) => "2",
            (1, 2) => "6",
            (1, 3) => "A",
            (2, 0) => "1",
            (2, 1) => "3",
            (2, 2) => "7",
            (2, 3) => "B",
            (2, 4) => "D",
            (3, 1) => "4",
            (3, 2) => "8",
            (3, 3) => "C",
            (4, 2) => "9",
            _ => panic!("Invalid position"),
        };
    }

    code
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 74921);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), "A6B35".to_string());
    }
}
