#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 20;

#[derive(Debug)]
pub struct Addr {
    start: u32,
    stop: u32,
}

impl Addr {
    pub fn in_range(&self, val: u32) -> bool {
        val >= self.start && val <= self.stop
    }
}

pub fn part1() -> u32 {
    let mut input = parser::AddrsParser::new().parse(&get_input!()).unwrap();
    input.sort_by_key(|a| a.start);
    let mut lowest = 0;

    loop {
        let (in_range, new_input): (Vec<_>, Vec<_>) =
            input.into_iter().partition(|a| a.in_range(lowest));
        if in_range.is_empty() {
            break;
        }
        lowest = in_range.iter().max_by_key(|a| a.stop).unwrap().stop + 1;
        input = new_input;
    }

    lowest
}

pub fn part2() -> u32 {
    0
    // let mut input = parser::AddrsParser::new().parse(&get_input!()).unwrap();
    // input.sort_by_key(|a| a.start);
    // let mut lowest = 0;
    // let mut count = 0;

    // while !input.is_empty() {
    //     let (in_range, new_input): (Vec<_>, Vec<_>) =
    //         input.into_iter().partition(|a| a.in_range(lowest));
    //     input = new_input;

    //     if !in_range.is_empty() {
    //         lowest = in_range.iter().max_by_key(|a| a.stop).unwrap().stop + 1;
    //     }

    //     if in_range.is_empty() && !input.is_empty() {
    //         input.retain(|ipt| ipt.start > lowest);
    //         let new_lowest = input.first().unwrap().start;
    //         count += new_lowest - lowest;
    //         lowest = new_lowest + 1
    //     }
    // }

    // count + u32::MAX - lowest
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 14975795);
    }

    // #[test]
    // fn test_part2() {
    //     assert_eq!(part2(), 0);
    // }
}
