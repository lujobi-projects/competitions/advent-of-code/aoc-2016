use std::collections::HashSet;

use utils::bfs;
use utils::*;

const CURRENT_DAY: u8 = 13;

type Postion = (i64, i64);

fn isfree((x, y): &Postion, magic_input: i64) -> bool {
    if *x < 0 || *y < 0 {
        return false;
    }
    let calc = x * x + 3 * x + 2 * x * y + y + y * y + magic_input;
    calc.count_ones() % 2 == 0
}

type StepFun = Box<dyn Fn(Postion) -> Postion>;
struct Steps {
    steps: Vec<(usize, StepFun)>,
    goal: Postion,
    magic_input: i64,
}

#[derive(Clone, Hash, Eq, PartialEq)]
struct State {
    value: Postion,
}

impl bfs::SearchState for State {
    type Global = Steps;
    type Label = usize;

    fn label(&self) -> Self::Label {
        (self.value.0 * 100000 + self.value.1) as usize
    }

    fn is_goal(&self, global: &Self::Global) -> bool {
        self.value == global.goal
    }

    fn successors(&self, global: &Self::Global) -> Vec<(usize, Box<Self>)> {
        let mut result = Vec::new();
        for &(cost, ref step) in global.steps.iter() {
            let pos = step(self.value);
            if !isfree(&pos, global.magic_input) {
                continue;
            }
            let next_state = State { value: pos };
            result.push((cost, Box::new(next_state)));
        }
        result
    }
}

fn search(magic_input: i64, goal: Postion) -> usize {
    let steps = Steps {
        steps: vec![
            (1, Box::new(|(x, y)| (x + 1, y))),
            (1, Box::new(|(x, y)| (x - 1, y))),
            (1, Box::new(|(x, y)| (x, y + 1))),
            (1, Box::new(|(x, y)| (x, y - 1))),
        ],
        goal,
        magic_input,
    };

    let start = State { value: (1, 1) };

    match bfs::bfs(&steps, &start) {
        Some((cost, _path)) => cost,
        None => {
            panic!("no solution")
        }
    }
}

fn visit_many(magic_input: i64, max_steps: usize, start: Postion, visited: &mut HashSet<Postion>) {
    if max_steps == 0 {
        return;
    }

    for pos in [
        (start.0 + 1, start.1),
        (start.0 - 1, start.1),
        (start.0, start.1 + 1),
        (start.0, start.1 - 1),
    ]
    .iter()
    {
        if !isfree(pos, magic_input) || visited.contains(pos) {
            continue;
        }
        visit_many(magic_input, max_steps - 1, *pos, visited);
    }
    visited.insert(start);
}

pub fn part1() -> i64 {
    let input = read_input!("\n", i64);
    let a = search(input[0], (31, 39)) as i64;
    println!("a:{}", a);
    0
}
pub fn part2() -> i64 {
    let input = read_input!("\n", i64);
    let mut visited = HashSet::new();
    visit_many(input[0], 50, (1, 1), &mut visited);
    print!("{:?}", visited);
    // visited.len() as i64 //110 too low
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 0);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 0);
    }
}
