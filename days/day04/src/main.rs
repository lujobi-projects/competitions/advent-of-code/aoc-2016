use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 4;

#[derive(Debug, PartialEq)]
struct Room {
    name: String,
    sector_id: i64,
    checksum: String,
}

impl Room {
    fn new(name: String, sector_id: i64, checksum: String) -> Self {
        Room {
            name,
            sector_id,
            checksum,
        }
    }
}

fn parse_rooms(ipt: Vec<Vec<String>>) -> Vec<Room> {
    ipt.iter()
        .map(|l| {
            let [names @ .., rest] = l.as_slice() else {
                panic!("Invalid input");
            };
            let name = names.join("-");
            let s = rest
                .trim_end_matches(']')
                .split('[')
                .clone()
                .collect::<Vec<_>>();
            let [sector_id, checksum] = s
                .as_slice() else {
                    panic!("Invalid input");
                };
            Room::new(
                name,
                sector_id.parse::<i64>().unwrap(),
                checksum.to_string(),
            )
        })
        .collect()
}

fn filter_decoy(rooms: Vec<Room>) -> Vec<Room> {
    rooms
        .into_iter()
        .filter(|room| {
            let counts =
                room.name
                    .chars()
                    .filter(|c| *c != '-')
                    .fold(HashMap::new(), |mut acc, c| {
                        *acc.entry(c).or_insert(0) += 1;
                        acc
                    });
            let mut counts = counts.into_iter().collect::<Vec<_>>();
            counts.sort_by_key(|a| a.0);
            counts.sort_by_key(|a| -a.1);

            counts
                .iter()
                .zip(room.checksum.chars())
                .all(|((c, _), cs)| *c == cs)
        })
        .collect()
}

pub fn part1() -> i64 {
    let input = read_input!("\n", "-", String);
    let rooms = parse_rooms(input);

    filter_decoy(rooms)
        .iter()
        .map(|room| room.sector_id)
        .sum::<i64>()
}
pub fn part2() -> i64 {
    let input = read_input!("\n", "-", String);
    let rooms = parse_rooms(input);

    filter_decoy(rooms)
        .iter()
        .find(|room| {
            room.name
                .chars()
                .map(|c| {
                    if c == '-' {
                        ' '
                    } else {
                        (((c as u64 - 'a' as u64 + room.sector_id as u64) % 26) as u8 + b'a')
                            as char
                    }
                })
                .collect::<String>()
                == "northpole object storage"
        })
        .unwrap()
        .sector_id
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 137896);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 501);
    }
}
