use utils::*;

const CURRENT_DAY: u8 = 9;

pub fn part1() -> i64 {
    let input = get_input!();
    // print!("{:?}", input);
    let mut count = 0;

    let mut chars = input.chars();

    loop {
        match chars.next() {
            None => break,
            Some('(') => {
                let mut x = String::new();
                let mut y = String::new();
                let mut c = chars.next().unwrap();
                while c != 'x' {
                    x.push(c);
                    c = chars.next().unwrap();
                }
                c = chars.next().unwrap();
                while c != ')' {
                    y.push(c);
                    c = chars.next().unwrap();
                }
                let x = x.parse::<usize>().unwrap();
                let y = y.parse::<usize>().unwrap();
                for _ in 0..x {
                    chars.next();
                }
                count += x * y;
            }
            Some(_) => count += 1,
        }
    }

    count as i64
}

fn decompress_substring(input: &str) -> usize {
    let mut count = 0;

    let mut chars = input.chars();

    loop {
        match chars.next() {
            None => break,
            Some('(') => {
                let mut x = String::new();
                let mut y = String::new();
                let mut c = chars.next().unwrap();
                while c != 'x' {
                    x.push(c);
                    c = chars.next().unwrap();
                }
                c = chars.next().unwrap();
                while c != ')' {
                    y.push(c);
                    c = chars.next().unwrap();
                }
                let x = x.parse::<usize>().unwrap();
                let y = y.parse::<usize>().unwrap();
                let mut substring = String::new();
                for _ in 0..x {
                    substring.push(chars.next().unwrap());
                }
                count += decompress_substring(&substring) * y;
            }
            Some(_) => count += 1,
        }
    }
    count
}

pub fn part2() -> i64 {
    let input = get_input!();
    decompress_substring(&input) as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 74532);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 11558231665);
    }
}
