use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 10;

#[derive(Debug, Clone, Copy)]
enum Output {
    Bot(usize),
    Output(usize),
}

#[derive(Debug, Clone)]
struct Bot {
    id: usize,
    out_low: Output,
    out_high: Output,
    chips: Vec<usize>,
}

type Bots = HashMap<usize, Bot>;

fn load_state(lines: Vec<String>) -> Bots {
    let mut bots = HashMap::new();
    for line in lines {
        let mut words = line.split_whitespace();
        match words.next() {
            Some("value") => {
                let value = words.next().unwrap().parse::<usize>().unwrap();
                let bot_id = words.nth(3).unwrap().parse::<usize>().unwrap();
                bots.entry(bot_id)
                    .or_insert(Bot {
                        id: bot_id,
                        out_low: Output::Bot(0),
                        out_high: Output::Bot(0),
                        chips: Vec::new(),
                    })
                    .chips
                    .push(value);
            }
            Some("bot") => {
                let bot_id = words.next().unwrap().parse::<usize>().unwrap();
                let out_low = match words.nth(3).unwrap() {
                    "bot" => Output::Bot(words.next().unwrap().parse::<usize>().unwrap()),
                    "output" => Output::Output(words.next().unwrap().parse::<usize>().unwrap()),
                    a => panic!("Unknown output type: {}", a),
                };
                let out_high = match words.nth(3).unwrap() {
                    "bot" => Output::Bot(words.next().unwrap().parse::<usize>().unwrap()),
                    "output" => Output::Output(words.next().unwrap().parse::<usize>().unwrap()),
                    _ => panic!("Unknown output type"),
                };
                let b = bots.entry(bot_id).or_insert(Bot {
                    id: bot_id,
                    out_low,
                    out_high,
                    chips: Vec::new(),
                });
                b.out_high = out_high;
                b.out_low = out_low;
            }
            _ => break,
        }
    }

    bots
}

pub fn part1() -> i64 {
    let input = read_input!("\n", String);
    let mut bots = load_state(input);

    loop {
        let mut changed = false;
        let mut new_bots = bots.clone();
        for bot in bots.values() {
            if bot.chips.len() == 2 {
                changed = true;
                let low = bot.chips.iter().min().unwrap();
                let high = bot.chips.iter().max().unwrap();
                if *low == 17 && *high == 61 {
                    return bot.id as i64;
                }
                if let Output::Bot(id) = bot.out_low {
                    new_bots.get_mut(&id).unwrap().chips.push(*low);
                }
                if let Output::Bot(id) = bot.out_high {
                    new_bots.get_mut(&id).unwrap().chips.push(*high);
                }
                new_bots.get_mut(&bot.id).unwrap().chips.clear();
            }
        }
        bots = new_bots;
        if !changed {
            panic!("No bots changed");
        }
    }
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);
    let mut bots = load_state(input);
    let mut outputs = HashMap::new();

    loop {
        let mut changed = false;
        let mut new_bots = bots.clone();
        for bot in bots.values() {
            if bot.chips.len() == 2 {
                changed = true;
                let low = bot.chips.iter().min().unwrap();
                let high = bot.chips.iter().max().unwrap();
                if let Output::Bot(id) = bot.out_low {
                    new_bots.get_mut(&id).unwrap().chips.push(*low);
                } else if let Output::Output(id) = bot.out_low {
                    outputs.entry(id).or_insert(vec![]).push(*low);
                }
                if let Output::Bot(id) = bot.out_high {
                    new_bots.get_mut(&id).unwrap().chips.push(*high);
                } else if let Output::Output(id) = bot.out_high {
                    outputs.entry(id).or_insert(vec![]).push(*high);
                }
                new_bots.get_mut(&bot.id).unwrap().chips.clear();
            }
        }
        bots = new_bots;
        if !changed {
            break;
        }
    }
    (outputs.get(&0).unwrap()[0] * outputs.get(&1).unwrap()[0] * outputs.get(&2).unwrap()[0]) as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 161);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 133163);
    }
}
