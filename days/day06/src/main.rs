use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 6;

pub fn part1() -> String {
    let input = read_input!("\n", String);
    let ipt_widt = input[0].chars().count();

    let mut trans_input = vec![vec![]; ipt_widt];

    for l in input {
        for (p, c) in l.chars().enumerate() {
            trans_input[p].push(c);
        }
    }

    trans_input
        .into_iter()
        .map(|v| {
            v.into_iter()
                .fold(HashMap::new(), |acc, c| {
                    let mut acc = acc;
                    *acc.entry(c).or_insert(0) += 1;
                    acc
                })
                .into_iter()
                .max_by_key(|&(_, v)| v)
                .unwrap()
                .0
        })
        .collect()
}

pub fn part2() -> String {
    let input = read_input!("\n", String);
    let ipt_widt = input[0].chars().count();

    let mut trans_input = vec![vec![]; ipt_widt];

    for l in input {
        for (p, c) in l.chars().enumerate() {
            trans_input[p].push(c);
        }
    }

    trans_input
        .into_iter()
        .map(|v| {
            v.into_iter()
                .fold(HashMap::new(), |acc, c| {
                    let mut acc = acc;
                    *acc.entry(c).or_insert(0) += 1;
                    acc
                })
                .into_iter()
                .min_by_key(|&(_, v)| v)
                .unwrap()
                .0
        })
        .collect()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), "qoclwvah".to_string());
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), "ryrgviuv".to_string());
    }
}
