use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 16;

fn checksum_disk(mut bools: Vec<bool>, len: usize) -> Vec<bool> {
    while bools.len() < len {
        let mut new_bools = Vec::new();
        for b in bools.iter() {
            new_bools.push(*b);
        }
        new_bools.push(false);
        for b in bools.iter().rev() {
            new_bools.push(!*b);
        }
        bools = new_bools;
    }
    bools = bools[0..len].to_vec();

    while bools.len() % 2 == 0 {
        let mut result = Vec::new();
        for i in (0..bools.len()).step_by(2) {
            result.push(bools[i] == bools[i + 1]);
        }
        bools = result;
    }
    bools
}

pub fn part1() -> String {
    let input = read_input!();
    let bools = input.iter().map(|x| x == &'1').collect_vec();

    checksum_disk(bools, 272)
        .iter()
        .map(|x| if *x { '1' } else { '0' })
        .collect()
}

pub fn part2() -> String {
    let input = read_input!();
    let bools = input.iter().map(|x| x == &'1').collect_vec();

    checksum_disk(bools, 35651584)
        .iter()
        .map(|x| if *x { '1' } else { '0' })
        .collect()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), String::from("11111000111110000"));
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), String::from("10111100110110100"));
    }
}
