use utils::*;

const CURRENT_DAY: u8 = 14;

fn calc_hash(salt: &str, index: i64) -> String {
    let digest = md5::compute(format!("{}{}", salt, index));
    format!("{:x}", digest)
}

fn calc_hash_2(salt: &str, index: i64) -> String {
    let digest = md5::compute(format!("{}{}", salt, index));
    let mut digest = format!("{:x}", digest);

    for _ in 0..2016 {
        digest = format!("{:x}", md5::compute(digest));
    }
    digest
}

fn logic(hash_fun: &dyn Fn(&str, i64) -> String) -> i64 {
    let input = read_input!("\n", String);

    let salt = input[0].clone();

    let mut keys = (0..1001)
        .map(|index| hash_fun(&salt, index))
        .collect::<Vec<_>>();

    let mut index = 1000;
    let mut found = 0;
    loop {
        let new_hash = hash_fun(&salt, index);

        // remove the first hash
        let hash = keys.remove(0);

        // add the new hash
        keys.push(new_hash);

        let aaaa = hash.chars().collect::<Vec<_>>();

        // find if there is a triple
        let triple = aaaa.windows(3).find(|w| w[0] == w[1] && w[1] == w[2]);

        if let Some(triple) = triple {
            let tofind = triple[0].to_string().repeat(5);
            // find if there is a quintuple
            let quintuple = keys
                .iter()
                .enumerate()
                .find(|(_, key)| key.contains(tofind.as_str()));

            if quintuple.is_some() {
                if found == 63 {
                    return index - 1001;
                } else {
                    found += 1;
                }
            }
        }
        index += 1;
    }
}

pub fn part1() -> i64 {
    logic(&calc_hash)
}
pub fn part2() -> i64 {
    logic(&calc_hash_2)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 23769);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 20606);
    }
}
