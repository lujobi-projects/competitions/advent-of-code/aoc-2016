use utils::bfs::{self, SearchState};
use utils::*;

const CURRENT_DAY: u8 = 17;

type Postion = (i64, i64);
struct Maze {
    goal: Postion,
}

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
struct State {
    value: Postion,
    step_with_pass: String,
}

impl bfs::SearchState for State {
    type Global = Maze;
    type Label = String;

    fn label(&self) -> Self::Label {
        self.step_with_pass.clone()
    }

    fn is_goal(&self, global: &Self::Global) -> bool {
        self.value == global.goal
    }

    fn successors(&self, _global: &Self::Global) -> Vec<(usize, Box<Self>)> {
        let mut result = Vec::new();

        let hash = md5::compute(self.step_with_pass.as_bytes());
        let hash = format!("{:x}", hash);

        for (i, (x_offs, y_offs, name)) in [(0, -1, 'U'), (0, 1, 'D'), (-1, 0, 'L'), (1, 0, 'R')]
            .iter()
            .enumerate()
        {
            let pos = (self.value.0 + x_offs, self.value.1 + y_offs);
            if pos.0 < 0 || pos.1 < 0 || pos.0 > 3 || pos.1 > 3 {
                continue;
            }
            if !hash.chars().nth(i).unwrap().is_digit(11) {
                continue;
            }
            result.push((
                1,
                Box::new(State {
                    value: pos,
                    step_with_pass: format!("{}{}", self.step_with_pass, name),
                }),
            ));
        }

        result
    }
}

pub fn part1() -> String {
    let input = read_input!().iter().collect::<String>();
    let ipt_len = input.len();

    let maze = Maze { goal: (3, 3) };

    let start = State {
        value: (0, 0),
        step_with_pass: input,
    };

    match bfs::bfs(&maze, &start) {
        Some((_, path)) => path.last().unwrap()[ipt_len..].to_string(),
        None => panic!("no solution"),
    }
}

fn find_max(path: &mut Vec<State>, maze: &Maze) -> usize {
    let last = path.last().unwrap();
    if last.is_goal(maze) {
        if path.len() < 8 {
            println!("{:?}", path);
        }
        return path.len();
    }

    let mut max = 0;

    let succccs = last.successors(maze);
    println!("{:?}", succccs);

    for (_, s) in succccs {
        path.push(*s);
        let dist = find_max(path, maze);
        if dist > max {
            println!("{}", dist);
        }
        path.pop();
        max = max.max(dist);
    }
    max
}

pub fn part2() -> usize {
    let input = read_input!().iter().collect::<String>();

    let maze = Maze { goal: (3, 3) };

    let start = State {
        value: (0, 0),
        step_with_pass: input,
    };

    let mut path = vec![start];
    find_max(&mut path, &maze)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    // print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    // use super::*;

    // #[test]
    // fn test_part1() {
    //     assert_eq!(part1(), "RDRLDRDURD".to_string());
    // }

    // #[test]
    // fn test_part2() {
    //     assert_eq!(part2(), 0);
    // }
}
