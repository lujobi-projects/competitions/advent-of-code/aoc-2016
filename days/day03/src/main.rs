use utils::*;

const CURRENT_DAY: u8 = 3;

pub fn part1() -> i64 {
    let input = read_input!("\n", String);

    let triangles = input.iter().map(|line| {
        line.trim()
            .split("  ")
            .filter(|s| !s.is_empty())
            .map(|s| s.trim().parse::<i64>().unwrap())
            .collect::<Vec<_>>()
    });
    triangles
        .filter(|t| t[0] + t[1] > t[2] && t[0] + t[2] > t[1] && t[2] + t[1] > t[0])
        .count() as i64
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);

    let triangles = input.chunks(3).flat_map(|lines| {
        let tris = lines
            .iter()
            .map(|line| {
                line.trim()
                    .split("  ")
                    .filter(|s| !s.is_empty())
                    .map(|s| s.trim().parse::<i64>().unwrap())
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        vec![
            vec![tris[0][0], tris[1][0], tris[2][0]],
            vec![tris[0][1], tris[1][1], tris[2][1]],
            vec![tris[0][2], tris[1][2], tris[2][2]],
        ]
    });
    triangles
        .filter(|t| t[0] + t[1] > t[2] && t[0] + t[2] > t[1] && t[2] + t[1] > t[0])
        .count() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1050);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 1921);
    }
}
