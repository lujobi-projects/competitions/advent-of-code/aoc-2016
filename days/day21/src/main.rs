#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 21;

/*
swap position X with position Y
swap letter X with letter Y
rotate left/right X steps
rotate based on position of letter X
reverse positions X through Y
*/

#[derive(Debug)]
pub enum Direction {
    Left,
    Right,
}
#[derive(Debug)]
pub enum Instruction {
    SwapPosition(usize, usize),
    SwapLetter(char, char),
    Rotate(Direction, usize),
    RotatePositions(char),
    ReversePositions(usize, usize),
    Move(usize, usize),
}

pub fn part1() -> i64 {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();
    print!("{:?}", input);
    0
}

pub fn part2() -> i64 {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();
    print!("{:?}", input);
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 0);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 0);
    }
}
