use utils::*;

const CURRENT_DAY: u8 = 11;

#[derive(Debug)]
enum Elements {
    Hydrogen,
    Lithium,
}

enum Parts {
    Generator(Elements),
    Microchip(Elements),
}

pub fn part1() -> i64 {
    let state = [
        [
            Parts::Microchip(Elements::Hydrogen),
            Parts::Microchip(Elements::Lithium),
        ],
        [
            Parts::Generator(Elements::Hydrogen),
            Parts::Generator(Elements::Lithium),
        ],
    ];

    if let Parts::Microchip(a) = &state[0][0] {
        print!("{:?}", a)
    }
    if let Parts::Generator(a) = &state[0][0] {
        print!("{:?}", a)
    }

    0
}
pub fn part2() -> i64 {
    // let input = read_input!("\n", ",", i64);
    print!("{:?}", 0);
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 0);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 0);
    }
}
