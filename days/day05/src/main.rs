use utils::*;

const CURRENT_DAY: u8 = 5;

pub fn part1() -> String {
    let input = get_input!();

    let mut good_pwd = vec![];
    let mut ct = 0;

    for i in 0.. {
        let password = format!("{}{}", input, i);
        let hash = md5::compute(password);
        let hash = format!("{:x}", hash);
        if hash.starts_with("00000") {
            good_pwd.push(hash);
            ct += 1;
            if ct == 8 {
                break;
            }
        }
    }

    good_pwd.iter().map(|s| s.chars().nth(5).unwrap()).collect()
}
pub fn part2() -> String {
    let input = get_input!();

    let mut pwd = [' '; 8];

    for i in 0.. {
        let password = format!("{}{}", input, i);
        let hash = md5::compute(password);
        let hash = format!("{:x}", hash);
        if hash.starts_with("00000") {
            let pos = hash.chars().nth(5).unwrap();
            let pos = pos.to_digit(16).unwrap();
            if pos < 8 && pwd[pos as usize] == ' ' {
                let c = hash.chars().nth(6).unwrap();
                pwd[pos as usize] = c;
                if pwd.iter().all(|c| *c != ' ') {
                    break;
                }
            }
        }
    }

    pwd.iter().collect()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    // use super::*;

    #[test]
    fn test_part1() {
        // assert_eq!(part1(), "f77a0e6e".to_string());
    }

    #[test]
    fn test_part2() {
        // assert_eq!(part2(), "999828ec".to_string());
    }
}
