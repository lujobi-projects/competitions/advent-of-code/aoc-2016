#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 12;

#[derive(Debug, Clone)]
pub enum Register {
    A,
    B,
    C,
    D,
}

#[derive(Debug, Clone)]
pub enum Value {
    Number(i64),
    Register(Register),
}

#[derive(Debug, Clone)]
pub enum Instruction {
    Copy(Value, Register),
    Increment(Register),
    Decrement(Register),
    JumpNotZero(Value, i64),
}

pub type Instructions = Vec<Instruction>;

type Registers = [i64; 4];

pub fn execute(ins: &[Instruction], regs: Registers) -> Registers {
    let end = ins.len();
    let mut registers = regs;
    let mut pc = 0;

    while pc < end {
        match ins[pc].clone() {
            Instruction::Copy(Value::Number(n), reg) => {
                registers[reg as usize] = n;
            }
            Instruction::Copy(Value::Register(reg), reg2) => {
                registers[reg2 as usize] = registers[reg as usize];
            }
            Instruction::Increment(reg) => {
                registers[reg as usize] += 1;
            }
            Instruction::Decrement(reg) => {
                registers[reg as usize] -= 1;
            }
            Instruction::JumpNotZero(Value::Number(n), offset) => {
                if n != 0 {
                    pc = (pc as i64 + offset) as usize;
                    continue;
                }
            }
            Instruction::JumpNotZero(Value::Register(reg), offset) => {
                if registers[reg as usize] != 0 {
                    pc = (pc as i64 + offset) as usize;
                    continue;
                }
            }
        }

        pc += 1;
    }
    registers
}

pub fn part1() -> i64 {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();

    let registers = execute(&input, [0; 4]);
    registers[Register::A as usize]
}

pub fn part2() -> i64 {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();

    let registers = execute(&input, [0, 0, 1, 0]);
    registers[Register::A as usize]
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 317993);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 9227647);
    }
}
