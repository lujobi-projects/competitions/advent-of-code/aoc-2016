use utils::*;

const CURRENT_DAY: u8 = 7;

fn is_ipv7(s: &str) -> bool {
    let mut nesting_level = 0;
    let mut found = false;
    let s_chars = s.chars().collect::<Vec<char>>();

    for i in 0..s.len() - 3 {
        let c = s_chars[i];
        if c == '[' {
            nesting_level += 1;
        } else if c == ']' {
            nesting_level -= 1;
        } else if i < s.len() - 3 {
            let a = s_chars[i];
            let b = s_chars[i + 1];
            let c = s_chars[i + 2];
            let d = s_chars[i + 3];

            if a == d && b == c && a != b {
                // if nesting_level % 2 == 0 {
                //     return false;
                // }
                if nesting_level != 0 {
                    return false;
                } else {
                    found = true;
                }
            }
        }
    }
    found
}

fn contains_in_brackets(a: char, b: char, s: &str) -> bool {
    let mut nesting_level = 0;
    let mut found = false;
    let s_chars = s.chars().collect::<Vec<char>>();

    for i in 0..s.len() - 2 {
        let c = s_chars[i];
        if c == '[' {
            nesting_level += 1;
        } else if c == ']' {
            nesting_level -= 1;
        }
        if c == a && s_chars[i + 1] == b && s_chars[i + 2] == a && nesting_level == 1 {
            found = true;
        }
    }
    found
}

pub fn part1() -> i64 {
    let input = read_input!("\n", String);
    input.iter().filter(|s| is_ipv7(s)).count() as i64
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);
    input
        .iter()
        .filter(|s| {
            let mut nesting_level = 0;
            let s_chars = s.chars().collect::<Vec<char>>();

            for i in 0..s.len() - 2 {
                let a = s_chars[i];
                if a == '[' {
                    nesting_level += 1;
                    continue;
                } else if a == ']' {
                    nesting_level -= 1;
                    continue;
                }
                let b = s_chars[i + 1];
                let c = s_chars[i + 2];
                if nesting_level == 0
                    && a == c
                    && a != b
                    && a != ']'
                    && b != ']'
                    && c != ']'
                    && a != '['
                    && b != '['
                    && c != '['
                    && contains_in_brackets(b, a, s)
                {
                    return true;
                }
            }
            false
        })
        .count() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2()); // 378 too high
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 118);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 260);
    }
}
