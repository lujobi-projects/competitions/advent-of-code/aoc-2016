#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 8;

#[derive(Debug)]
pub enum Instruction {
    Rect(usize, usize),
    RotateRow(usize, usize),
    RotateColumn(usize, usize),
}

fn process_instructions(insns: Vec<Instruction>) -> Vec<Vec<bool>> {
    let mut screen = vec![vec![false; 50]; 6];

    for ins in insns {
        match ins {
            Instruction::Rect(x, y) => {
                for row in screen.iter_mut().take(y) {
                    for cell in row.iter_mut().take(x) {
                        *cell = true;
                    }
                }
            }
            Instruction::RotateRow(y, n) => {
                let mut row = screen[y].clone();
                for i in 0..50 {
                    row[(i + n) % 50] = screen[y][i];
                }
                screen[y] = row;
            }
            Instruction::RotateColumn(x, n) => {
                let mut col = [false; 6];
                for i in 0..6 {
                    col[(i + n) % 6] = screen[i][x];
                }
                for i in 0..6 {
                    screen[i][x] = col[i];
                }
            }
        }
    }
    screen
}

pub fn part1() -> i64 {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();

    let screen = process_instructions(input);
    screen.iter().flatten().filter(|&&x| x).count() as i64
}

pub fn part2() -> String {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();

    let screen = process_instructions(input);

    let mut s = String::new();
    s.push('\n');
    for row in screen {
        for col in row {
            if col {
                s.push('#');
            } else {
                s.push(' ');
            }
        }
        s.push('\n');
    }
    s
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 115);
    }

    #[test]
    fn test_part2() {
        let res = String::from(
            "
#### #### #### #   ##  # #### ###  ####  ###   ## 
#    #    #    #   ## #  #    #  # #      #     # 
###  ###  ###   # # ##   ###  #  # ###    #     # 
#    #    #      #  # #  #    ###  #      #     # 
#    #    #      #  # #  #    # #  #      #  #  # 
#### #    ####   #  #  # #    #  # #     ###  ##  
",
        );
        // EFEYKFRFIJ
        assert_eq!(part2(), res);
    }
}
