#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 15;

#[derive(Debug)]
pub struct Disc {
    index: u128,
    position_count: u128,
    position: u128,
}

pub fn chinese_remainder_solver(a_ns: Vec<(u128, u128)>) -> u128 {
    let n: u128 = a_ns.iter().map(|x| x.1).product();
    let mut sum = 0;

    for a in a_ns {
        let m = a.1;
        let z = n / m;
        let y = modinverse((z % m) as i128, m as i128).unwrap() as u128;
        let w = (y * z) % n;
        sum += (a.0 * w) % n;
    }
    sum % n
}

pub fn egcd(a: i128, b: i128) -> (i128, i128, i128) {
    if a == 0 {
        (b, 0, 1)
    } else {
        let (g, x, y) = egcd(b % a, a);
        (g, y - (b / a) * x, x)
    }
}
pub fn modinverse(a: i128, m: i128) -> Option<i128> {
    let (g, x, _) = egcd(a, m);
    if g != 1 {
        None
    } else {
        Some((x % m + m) % m)
    }
}

pub fn part1() -> u128 {
    let input = parser::DiscsParser::new().parse(&get_input!()).unwrap();

    let mut a_ns = Vec::new();
    for disc in input {
        let d_ct = disc.position_count;
        a_ns.push(((2 * d_ct - disc.position - disc.index) % d_ct, d_ct));
    }
    chinese_remainder_solver(a_ns)
}

pub fn part2() -> u128 {
    let mut input = parser::DiscsParser::new().parse(&get_input!()).unwrap();
    input.push(Disc {
        index: input.len() as u128 + 1,
        position_count: 11,
        position: 0,
    });

    let mut a_ns = Vec::new();
    for disc in input {
        let d_ct = disc.position_count;
        a_ns.push(((2 * d_ct - disc.position - disc.index) % d_ct, d_ct));
    }

    chinese_remainder_solver(a_ns)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 16824);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 3543984);
    }
}
