use std::collections::HashSet;

use utils::*;

const CURRENT_DAY: u8 = 1;

pub fn part1() -> i64 {
    let input = read_input!(", ", String);

    let insns = input
        .iter()
        .map(|x| {
            let mut chars = x.chars();
            let op = chars.next().unwrap();
            let arg = chars.as_str().parse::<i64>().unwrap();
            (op, arg)
        })
        .collect::<Vec<_>>();

    let mut x_dir = 0;
    let mut y_dir = 1;
    let mut x_pos = 0;
    let mut y_pos = 0;

    for ins in insns {
        match ins {
            ('R', _) => {
                let temp = x_dir;
                x_dir = y_dir;
                y_dir = -temp;
            }
            ('L', _) => {
                let temp = x_dir;
                x_dir = -y_dir;
                y_dir = temp;
            }
            _ => panic!("Invalid direction"),
        }

        x_pos += x_dir * ins.1;
        y_pos += y_dir * ins.1;
    }

    x_pos.abs() + y_pos.abs()
}
pub fn part2() -> i64 {
    let input = read_input!(", ", String);

    let insns = input
        .iter()
        .map(|x| {
            let mut chars = x.chars();
            let op = chars.next().unwrap();
            let arg = chars.as_str().parse::<i64>().unwrap();
            (op, arg)
        })
        .collect::<Vec<_>>();

    let mut x_dir = 0;
    let mut y_dir = 1;
    let mut x_pos: i64 = 0;
    let mut y_pos: i64 = 0;

    let mut visited = HashSet::new();

    for ins in insns {
        match ins {
            ('R', _) => {
                let temp = x_dir;
                x_dir = y_dir;
                y_dir = -temp;
            }
            ('L', _) => {
                let temp = x_dir;
                x_dir = -y_dir;
                y_dir = temp;
            }
            _ => panic!("Invalid direction"),
        }

        for _ in 0..ins.1 {
            x_pos += x_dir;
            y_pos += y_dir;

            if visited.contains(&(x_pos, y_pos)) {
                return x_pos.abs() + y_pos.abs();
            }

            visited.insert((x_pos, y_pos));
        }
    }
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 243);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 142);
    }
}
