use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 18;

fn rule(left: &bool, center: &bool, right: &bool) -> bool {
    !(*center || *left && *right || !*left && !*right)
}

fn run(input: Vec<char>, rows: usize) -> usize {
    let len = input.len();
    let mut traps = input.iter().map(|x| *x == '^').collect_vec();
    let mut count = traps.iter().filter(|x| !*x).count();

    for _ in 0..(rows - 1) {
        let mut new_traps = vec![];

        new_traps.push(rule(&false, traps.first().unwrap(), traps.get(1).unwrap()));
        for t in 1..len - 1 {
            new_traps.push(rule(
                traps.get(t - 1).unwrap(),
                traps.get(t).unwrap(),
                traps.get(t + 1).unwrap(),
            ))
        }
        new_traps.push(rule(
            traps.get(len - 2).unwrap(),
            traps.get(len - 1).unwrap(),
            &false,
        ));

        traps = new_traps;
        count += traps.iter().filter(|x| !*x).count();
    }
    count
}

pub fn part1() -> usize {
    let input = read_input!();
    run(input, 40)
}
pub fn part2() -> usize {
    let input = read_input!();
    run(input, 400_000)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1()); // 2075 high
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_part1() {
        // assert_eq!(part1(), 2013);
    }

    #[test]
    fn test_part2() {
        // assert_eq!(part2(), 20006289);
    }
}
