use utils::*;

const CURRENT_DAY: u8 = 19;

//   1
// 5   2
//  4 3
//   0
// 4   1
//  3 2

// 0 1 2 3 4  |
// *          |
// 0   2   4  | (x+0) % (2^1)
// 0   1   2  | (x+0) % (2^1)
//         *  | (x+0) % (2^1)
//     2      | (x+2) % (2^2)

// 0 1 2 3 4 5 6 7 8 9  |
// 0   2   4   6   8    | (x+0) % (2^1)
// 0       4       8    | (x+0) % (2^1)
//         4
//

// 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
// 0   2   4   6   8   10    12    14
// 0       4       8         12
// 0               8

// 0 1 2 3 4 5
// 0   2   4
// 0       4
//         4

// 0 -> 0
// 1 -> 0
// 2 -> 2
// 3 -> 0
// 4 -> 2
// 5 -> 4

fn distribute_packets(last: usize, offset: usize) -> usize {
    if last == 0 {
        return 0;
    }
    let count = last + 1;
    let new_offset = count % 2;
    let inner = distribute_packets(last / 2, new_offset);
    (2 * inner + count - offset) % count
}

pub fn part1() -> usize {
    let input = read_input!("\n", usize);
    println!("{:?}", input);
    println!("{:?}", distribute_packets(0, 0));
    println!("{:?}", distribute_packets(1, 0));
    println!("{:?}", distribute_packets(2, 0));
    println!("{:?}", distribute_packets(3, 0));
    println!("{:?}", distribute_packets(4, 0));
    println!("{:?}", distribute_packets(5, 0));
    distribute_packets(*input.first().unwrap(), 0) + 1 // 1815605 high
}
pub fn part2() -> i64 {
    let input = read_input!("\n", i64);
    print!("{:?}", input);
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1815605);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 0);
    }
}
